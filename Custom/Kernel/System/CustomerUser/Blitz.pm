# --
# Copyright (C) 2001-2020 OTRS AG, https://otrs.com/
# --
# This software comes with ABSOLUTELY NO WARRANTY. For details, see
# the enclosed file COPYING for license information (GPL). If you
# did not receive this file, see https://www.gnu.org/licenses/gpl-3.0.txt.
# --

package Kernel::System::CustomerUser::Blitz;

use strict;
use warnings;

use Kernel::System::VariableCheck qw(:all);

use Crypt::PasswdMD5 qw(unix_md5_crypt apache_md5_crypt);
use Digest::SHA;

use Kernel::System::VariableCheck qw(:all);
use Data::Dumper;
use JSON::XS;
use Redis;

our @ObjectDependencies = (
    'Kernel::Config',
    'Kernel::Language',
    'Kernel::System::Cache',
    'Kernel::System::CheckItem',
    'Kernel::System::DateTime',
    'Kernel::System::DB',
    'Kernel::System::DynamicField',
    'Kernel::System::DynamicField::Backend',
    'Kernel::System::Encode',
    'Kernel::System::Log',
    'Kernel::System::Main',
    'Kernel::System::Valid',
    'Kernel::System::DynamicField',
    'Kernel::System::DynamicField::Backend',
    'Kernel::System::DynamicFieldValueObjectName',
);

sub new {
    my ( $Type, %Param ) = @_;
    my $Self = {};
    bless( $Self, $Type );

    # check needed data
    for my $Needed (qw( PreferencesObject CustomerUserMap )) {
        $Self->{$Needed} = $Param{$Needed} || die "Got no $Needed!";
    }

    # $Self->{Redis} = $Self->{CustomerUserMap}->{Params}->{Redis} || Redis->new();
    $Self->{Redis} = Redis->new();

    ENTRY:
    for my $Entry ( @{ $Self->{CustomerUserMap}->{Map} } ) {
        if ( $Entry->[0] eq 'UserLogin' && $Entry->[5] =~ /^int$/i ) {
            $Self->{CustomerKeyInteger} = 1;
            last ENTRY;
        }
    }

    return $Self;
}

sub CustomerName {
    my ( $Self, %Param ) = @_;

    # check needed stuff
    if ( !$Param{UserLogin} ) {
        $Kernel::OM->Get('Kernel::System::Log')->Log(
            Priority => 'error',
            Message  => 'Need UserLogin!',
        );
        return;
    }
    return $Param{FirstName} if defined $Param{FirstName};
    my $user;
    eval { $user = decode_json $Self->{Redis}->get($Param{UserLogin}) };
    $user->{given_name} ||= '';
    $user->{family_name} ||= '';

    return "$user->{given_name} $user->{family_name}" || 'unknown';
}

sub CustomerSearch {
    my ( $Self, %Param ) = @_;

    # $Kernel::OM->Get('Kernel::System::Log')->Log(
    #     Priority => 'error',
    #     Message => Dumper \%Param
    # );

    # Valid Limit
    my @keys;
    eval { @keys = $Self->{Redis}->keys($Param{Search} || $Param{UserLogin}) };
    return if $@;

    my %Users = map {$_ => "firstname lastname <$_>"} @keys;
    return %Users;
}

sub CustomerSearchDetail {
    my ( $Self, %Param ) = @_;

    return [ $Param{UserLogin} ];
}

sub CustomerIDList {
    my ( $Self, %Param ) = @_;

    return ['CustomerID_1'];
}

sub CustomerIDs {
    my ( $Self, %Param ) = @_;

    return ['CustomerID_1'];
}

sub CustomerUserDataGet {
    my ( $Self, %Param ) = @_; # User

    # $Kernel::OM->Get('Kernel::System::Log')->Log(
    #     Priority => 'error',
    #     Message => Dumper \@keys
    # );

    my $User;
    eval { $User = decode_json $Self->{Redis}->get("$Param{User}") };
    return if $@ || !%$User;

    my %Data = (
        UserID         => $User->{email},
        UserLogin      => $User->{email},
        UserTitle      => sprintf("%s %s", $User->{given_name}, $User->{family_name}),
        UserFullname   => sprintf("%s %s", $User->{given_name}, $User->{family_name}),
        UserFirstname  => $User->{given_name},
        UserLastname   => $User->{family_name},
        UserMailString => sprintf("%s %s <%s>", $User->{given_name}, $User->{family_name}, $User->{email}),
        UserEmail      => $User->{email},
        CreateTime  => '2020-01-01 10:10:10',
        CreateBy    => 1,
        ChangeTime  => '2020-01-01 10:10:10',
        ChangeBy    => 1,
        ValidID     => 1,
        UserCustomerID  => 'CustomerID_1'
    );

    my %Preferences = $Self->GetPreferences( UserID => $Data{UserID} );
    return ( %Data, %Preferences );
}

sub CustomerUserAdd {
    my ( $Self, %Param ) = @_;

    return;
}

sub CustomerUserUpdate {
    my ( $Self, %Param ) = @_;

    $Kernel::OM->Get('Kernel::System::Log')->Log(
        Priority => 'error',
        Message => Dumper \%Param
    );

    # my $User = $Self->{Redis}->exists($Param{User}) ? decode_json $Self->{Redis}->get($Param{UserLogin}) : {};
    my $User = decode_json $Self->{Redis}->get($Param{UserLogin});

    return unless %$User;

    map {$User->{$_} = $Param{$_}} keys %Param;

    $Kernel::OM->Get('Kernel::System::Log')->Log(
        Priority => 'error',
        Message => Dumper $User
    );

    $Self->{Redis}->set($User->{UserLogin}, encode_json $User);
    return;
}

sub SetPassword {
    my ( $Self, %Param ) = @_;

    return;
}

sub GenerateRandomPassword {
    my ( $Self, %Param ) = @_;

    # generated passwords are eight characters long by default
    my $Size = $Param{Size} || 8;

    my $Password = $Kernel::OM->Get('Kernel::System::Main')->GenerateRandomString(
        Length => $Size,
    );

    return $Password;
}

sub SetPreferences {
    my ( $Self, %Param ) = @_;

    # check needed params
    if ( !$Param{UserID} ) {
        $Kernel::OM->Get('Kernel::System::Log')->Log(
            Priority => 'error',
            Message  => 'Need UserID!',
        );
        return;
    }

    return $Self->{PreferencesObject}->SetPreferences(%Param);
}

sub GetPreferences {
    my ( $Self, %Param ) = @_;

    # check needed params
    if ( !$Param{UserID} ) {
        $Kernel::OM->Get('Kernel::System::Log')->Log(
            Priority => 'error',
            Message  => 'Need UserID!',
        );
        return;
    }

    return $Self->{PreferencesObject}->GetPreferences(%Param);
}


sub SearchPreferences {
    my ( $Self, %Param ) = @_;

    return $Self->{PreferencesObject}->SearchPreferences(%Param);
}

sub _CustomerUserCacheClear {
    my ( $Self, %Param ) = @_;
    return;
}

sub DESTROY {
    my $Self = shift;

    # disconnect if it's not a parent DBObject
    if ( $Self->{NotParentDBObject} ) {
        if ( $Self->{DBObject} ) {
            $Self->{DBObject}->Disconnect();
        }
    }

    return 1;
}

sub flog {
    my ($self, $msg) = @_;

    my $line = ( caller )[2];
    $Kernel::OM->Get('Kernel::System::Log')->Log(Priority => 'error', Message => ( caller )[2] );
    $Kernel::OM->Get('Kernel::System::Log')->Log(Priority => 'error', Message => Dumper $msg);
}

1;
