package Kernel::System::Net::Blitz;

our @ObjectDependencies = (
    'Kernel::Config',
    'Kernel::System::Log',
);

sub new {
    my ( $Type, %Param ) = @_;

    # allocate new hash for object
    my $Self = {};
    bless( $Self, $Type );

    # Debug 0=off 1=on
    $Self->{Debug} = 0;


    $Self->{Backend} = {
        'test1' => {
            User => 'test1',
            Pw   => 'qwerty1',
            UserFirstname => 'Test',
            UserLastname  => '1',
            UserEmail => 'test1@domain.com'
        },
        'test2' => {
            User => 'test2',
            Pw => 'qwerty2',
            UserFirstname => 'Test 2',
            UserLastname  => '2',
            UserEmail => 'test2@domain.com'
        },
        'test3' => {
            User => 'test3',
            Pw => 'qwerty3',
            UserFirstname => 'Test 3',
            UserLastname  => '2',
            UserEmail => 'test3@domain.com'
        },
    };

    return $Self;
}

sub check_pwd {
    my ( $Self, $User, $Pw ) = @_;

    return undef unless exists $Self->{Backend}->{$User};
    return $Self->{Backend}->{$User}->{Pw} eq $Pw ? 1 : undef;

}

sub sync_user_data {
    my ( $Self, $User ) = @_;

    return undef unless exists $Self->{Backend}->{$User};
    
    # my %data = %{ $Self->{Backend}->{$User} };
    return %{ $Self->{Backend}->{$User} };
}

1;
