package Kernel::Modules::PublicTicketCreate;

use strict;
use warnings;
use Data::Dumper;

our $ObjectManagerDisabled = 1;

use Kernel::System::VariableCheck qw(:all);
use Kernel::Language qw(Translatable);

sub new {
    my ( $Type, %Param ) = @_;

    # allocate new hash for object
    my $Self = {%Param};
    bless( $Self, $Type );

    return $Self;
}

sub Run {
    my ( $Self, %Param ) = @_;

    my $LayoutObject = $Kernel::OM->Get('Kernel::Output::HTML::Layout');
    my $ParamObject  = $Kernel::OM->Get('Kernel::System::Web::Request');
    my $TicketObject = $Kernel::OM->Get('Kernel::System::Ticket');
    my $QueueObject  = $Kernel::OM->Get('Kernel::System::Queue');

    my $DynamicFieldObject = $Kernel::OM->Get('Kernel::System::DynamicField');
    my $BackendObject      = $Kernel::OM->Get('Kernel::System::DynamicField::Backend');
    my $ArticleObject        = $Kernel::OM->Get('Kernel::System::Ticket::Article');
    my $ArticleBackendObject = $ArticleObject->BackendForChannel( ChannelName => 'Internal' );

    my $ConfigObject = $Kernel::OM->Get('Kernel::Config');


    my %Params = ();
    my %Errors = ();

    my %GetParams = map {$_ => $ParamObject->GetParam( Param => $_) || ''} $ParamObject->GetParamNames();

    my $Config = $ConfigObject->Get("Ticket::Frontend::$Self->{Action}");

    my $DynamicField = $DynamicFieldObject->DynamicFieldListGet(
        Valid       => 1,
        ObjectType  => [ 'Ticket', 'Article' ],
        FieldFilter => $Config->{DynamicField} || {},
    );

    $Self->log(\%GetParams);

    if ($GetParams{'Subaction'} eq 'StoreNew') {
        for my $DynamicFieldConfig (@$DynamicField) {
            my $Success = $BackendObject->ValueValidate(
                DynamicFieldConfig => $DynamicFieldConfig,
                Value              => $GetParams{ $DynamicFieldConfig->{Name} },
                UserID             => 1,
            );
        }
        # $Errors{SubjectInvalid} = 'ServerError';
        # my @Params = (
        #     'DynamicField_Region',
        #     'DynamicField_PublicTicketCreateOrganization',
        #     'DynamicField_PublicTicketCreateFamilyName',
        #     'DynamicField_PublicTicketCreateMiddleName',
        #     'DynamicField_PublicTicketCreateFirstName',
        #     'DynamicField_PublicTicketCreatePosition',
        #     'Subject',
        #     'Body'
        # );
        # 'Action',
        # 'Subaction',
        # 'DynamicField_Region',
        # 'DynamicField_PublicTicketCreateOrganization',
        # 'DynamicField_PublicTicketCreateFamilyName',
        # 'DynamicField_PublicTicketCreateMiddleName',
        # 'DynamicField_PublicTicketCreateFirstName',
        # 'DynamicField_PublicTicketCreatePosition',
        # 'Subject',
        # 'Body',
        # 'FileUpload'

        my $TicketID = $TicketObject->TicketCreate(
            QueueID      => 6, # 1-линия - тех поддержка
            TypeID       => 2, # Web заявка
            Title        => $GetParams{Subject},
            PriorityID   => 3, # 3 normal
            Lock         => 'unlock',
            State        => 'new',
            CustomerID   => $GetParams{Email},
            CustomerUser => $GetParams{Email},
            OwnerID      => 1,
            UserID       => 1,
            # ServiceID    => $GetParam{ServiceID},
            # SLAID        => $GetParam{SLAID},
        );

        if (0 && $TicketID) {
            my $From = sprintf("\"%s %s\" %s", 
                $GetParams{DynamicField_PublicTicketCreateFamilyName},
                $GetParams{DynamicField_PublicTicketCreateFirstName},
                $GetParams{Email}
            );

            my $PlainBody = $LayoutObject->{BrowserRichText} ? $LayoutObject->RichText2Ascii(String => $GetParam{Body}) : $GetParam{Body};

            my $ArticleID = $ArticleBackendObject->ArticleCreate(
                TicketID             => $TicketID,
                IsVisibleForCustomer => 1,
                SenderType           => 'customer',
                From                 => $From,
                To                   => 'to',
                Subject              => $GetParams{Subject},
                Body                 => $GetParams{Body},
                MimeType             => $LayoutObject->{BrowserRichText} ? 'text/html' : 'text/plain',
                Charset              => $LayoutObject->{UserCharset},
                UserID               => $ConfigObject->Get('CustomerPanelUserID'),
                HistoryType          => $Config->{HistoryType} || 'PublicTicketCreate',
                HistoryComment       => $Config->{HistoryComment} || '%%',
                AutoResponseType     => ( $ConfigObject->Get('AutoResponseForWebTickets') ) ? 'auto reply' : '',
                OrigHeader => {
                    From    => $From,
                    To      => 'to',
                    Subject => $GetParams{Subject},
                    Body    => $PlainBody
                },
                Queue => $QueueObject->QueueLookup( QueueID => 6 ),
            );
        }
    }

    for my $DynamicFieldConfig ( @{$DynamicField} ) {
        next if !IsHashRefWithData($DynamicFieldConfig);
        my $field_render = $BackendObject->EditFieldRender(
            DynamicFieldConfig   => $DynamicFieldConfig,
            Mandatory => $Config->{DynamicField}->{ $DynamicFieldConfig->{Name} } == 2,
            LayoutObject    => $LayoutObject,
            ParamObject     => $ParamObject,
        );

        $LayoutObject->Block(
            Name => 'DynamicField',
            Data => {
                Name  => $DynamicFieldConfig->{Name},
                Label => $field_render->{Label},
                Field => $field_render->{Field},
            },
        );
    }

    my $Output = $LayoutObject->CustomerHeader(
        Type  => '',
        Title => 'Ticket Create',
    );
    $Output .= $LayoutObject->Output(
        TemplateFile => 'PublicTicketCreate',
        Data => {
            %Errors
            # dump => Dumper(\%Params)
        }
    );
    $Output .= $LayoutObject->CustomerFooter( Type => '' );
    return $Output;
}

sub log {
    my ($self, $msg) = @_;

    $Kernel::OM->Get('Kernel::System::Log')->Log(
        Priority => 'error',
        Message  => Dumper $msg
    );
}

1;
