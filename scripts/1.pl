#!/usr/bin/perl

use strict;
use warnings;

# use ../ as lib location
use File::Basename;
use FindBin qw($RealBin);
use lib dirname($RealBin);
use lib dirname($RealBin) . "/Kernel/cpan-lib";
use Text::CSV_XS qw( csv );
use DDP;
use Kernel::System::ObjectManager;

local $Kernel::OM = Kernel::System::ObjectManager->new(
    'Kernel::System::Log' => {
        LogPrefix => 'OTRS-1.pl',
    },
);

my $CU = $Kernel::OM->Get('Kernel::System::CustomerUser');
my $CC = $Kernel::OM->Get('Kernel::System::CustomerCompany');

my $fu = csv(in => 'upload.csv', headers => 'auto');
my @skipped = ();

for my $user ( @$fu ) {
    print "processing: $user->{email}...\n";
    unless ($user->{CustomerID} && $user->{'email'}) {
        push @skipped, $user->{'email'};
        next;
    }
    print "step 1...\n";

    if (!$CC->CustomerCompanyGet(CustomerID => $user->{CustomerID})) {
        print $CC->CustomerCompanyAdd(
            CustomerID => $user->{CustomerID},
            CustomerCompanyName => $user->{CustomerID},
            UserID => 1,
            ValidID => 1
        ) ? "[INFO] Added CustomerID `$user->{CustomerID}`\n" :
            "[ERROR] Error Due creating `$user->{CustomerID}`\n";
    }

    print "step 2...\n";

    # my $r = Redis->new();
    # p $r;
    # $CU->CustomerUserDataGet( User => $user->{'email'} );
    # last;
    # p $@;
    # p $CU->get_object;

    if(!$CU->CustomerSearch(
        PostMasterSearch => $user->{'email'},
        Valid => 0
    )) {
        print $CU->CustomerUserAdd(
            UserFirstname  => $user->{FirstName},
            UserLastname   => $user->{FamilyName},
            UserMiddlename => $user->{MiddleName},
            UserCustomerID => $user->{CustomerID},
            UserLogin      => $user->{'email'},
            UserRegion     => $user->{Region},
            UserPosition   => $user->{Position},
            UserPhone      => $user->{Phone},
            UserMobile     => $user->{Phone1},
            UserDepartment => $user->{Department},
            # UserPassword   => '',
            UserEmail      => $user->{'email'},
            ValidID        => 1,
            UserID         => 1) ?
                "[INFO] Added CustomerUser `$user->{email}`\n" :
                "[ERROR] Error Due creating `$user->{email}`\n";
    }
    print "end...\n";
}

print "Was skiped: \n";
p @skipped;

