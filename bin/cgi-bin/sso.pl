#!/usr/bin/perl
# --

use strict;
use warnings;
use CGI;
use DDP;
use Data::Dumper;
use JSON::XS;
use POSIX;

# use ../../ as lib location
use FindBin qw($Bin);
use lib "$Bin/../..";
use lib "$Bin/../../Kernel/cpan-lib";
use lib "$Bin/../../Custom";

# 0=off;1=on;
my $Debug = 0;

# load agent web interface
# use Kernel::System::Web::InterfaceSSO();
use Kernel::System::ObjectManager;

local $Kernel::OM = Kernel::System::ObjectManager->new();

llog('SSO Started!');

my $query = CGI->new;
my $r = Redis->new( debug => 1 );
my $SessionObject = $Kernel::OM->Get('Kernel::System::AuthSession');

my %params = map { split '=', $_ } split '&', $ENV{QUERY_STRING};
$params{grant_type} = 'authorization_code';
$params{redirect_uri} = 'https://otrs.dev-mcx.ru/otrs/sso.pl';

llog(\%params);

if ( defined $params{code} ) {
    my $curl_1 = decode_json `curl -k -d  "grant_type=authorization_code&redirect_uri=$params{redirect_uri}&code=$params{code}" -H "Authorization: Basic cG9ydGFsLWJpdHJpeDpqTzBzZmE5Z0ljNG5aMDY=" -X POST https://login.tr.local/blitz/oauth/te`;
    my $curl_2  = `curl -k -v  https://login.tr.local/blitz/oauth/me -H \"Authorization: Bearer $curl_1->{access_token}"`;
    my $auth_hash = decode_json $curl_2;
    $auth_hash->{blitz_token} = $params{code};
    llog($auth_hash);
    $r->set($auth_hash->{email}, encode_json $auth_hash);

    my $SessionID = $SessionObject->CreateSessionID(
        ChangeBy => 1,
        ChangeTime => strftime("%F %T", localtime $^T),
        CompanyConfig => {},
        Config => {},
        CreateBy => 1,
        CreateTime => strftime("%F %T", localtime $^T),
        CustomerCompanyName => 'Внешние пользователи',
        CustomerCompanyValidID => 1,
        CustomerID => 'Внешние пользователи',
        SessionSource => 'CustomerInterface',
        Source => 'Blitz', # CustomerUser
        UserCustomerID => 'Внешние пользователи',
        UserEmail => $auth_hash->{email},
        UserFirstname => $auth_hash->{name},
        UserFullname => sprintf("%s %s %s", $auth_hash->{name}, $auth_hash->{middle_name}, $auth_hash->{family_name}),
        UserID => $auth_hash->{email},
        UserLanguage => 'ru',
        UserLastname => $auth_hash->{family_name},
        UserLogin => $auth_hash->{email},
        UserMailString => sprintf("%s %s <%s>", $auth_hash->{name}, $auth_hash->{family_name}, $auth_hash->{email}),
        UserPassword => '[xxx]',
        UserRefreshTime => 10,
        UserRemoteAddr => '31.185.7.89',
        UserTitle => $auth_hash->{email},
        UserSessionStart => time(),
        UserShowTickets => '25',
        UserTimeZone => 'Europe/Moscow',
        UserType  => 'Customer',
        UserLastRequest => time(),
        UserZip => '',
        ValidID => 0,
    );

    my $res = $SessionObject->CheckSessionID( SessionID => $SessionID ) || '';

    if ( $SessionObject->CheckSessionID( SessionID => $SessionID ) ) {
        print $query->header( -cookie  => [ "OTRSCustomerInterface=$SessionID" ]);
        print $query->redirect( "https://otrs.dev-mcx.ru/otrs/customer.pl?Action=CustomerTicketOverview;Subaction=MyTickets;OTRSCustomerInterface=$SessionID;res=$res" );
    } else {
        cgi_print('Error occur due creating session');
    }

}

sub cgi_print {
    my $str = shift;
    print "Content-Type: text/html\n\n";
    print "<html> <head>\n";
    print "<title>Hello, world!</title>";
    print "</head>\n";
    print "<body>\n";
    print "<h1>Hello, world!</h1>\n";
    print "<p>$str</p>";
    print "</body> </html>\n";
}

sub llog {
    $Kernel::OM->Get('Kernel::System::Log')->Log(
        Priority => 'error',
        Message => Dumper $_[0]
    );
}


    # ChangeBy	6
    # ChangeTime	2020-07-23 22:38:47
    # CompanyConfig	[...]
    # Config	[...]
    # CreateBy	1
    # CreateTime	2020-06-04 13:56:58
    # CustomerCompanyName	Внешние пользователи
    # CustomerCompanyValidID	1
    # CustomerID	Внешние пользователи
    # LastScreenOverview	Action=CustomerTicketOverview;Subaction=MyTickets
    # SessionID	eXfe82ATN9zy1pCs4OGn1V5TRLFDO3Xy
    # SessionSource	CustomerInterface
    # Source	CustomerUser
    # UserChallengeToken	fxuwCsxWXbE5wD3kmIZbunZ06CBqhPhA
    # UserCustomerID	Внешние пользователи
    # UserEmail	subject@dotbot.ru
    # UserFirstname	esiatest004
    # UserFullname	esiatest004@yandex.ru esiatest004 yandex.ru
    # UserID	esiatest004
    # UserLanguage	ru
    # UserLastLogin	1600544032
    # UserLastLoginTimestamp	2020-09-19 22:33:52
    # UserLastRequest	1600544032
    # UserLastname	yandex.ru
    # UserLogin	esiatest004
    # UserMailString	&quot;esiatest004 yandex.ru&quot; &lt;subject@dotbot.ru&gt;
    # UserPassword	[xxx]
    # UserRefreshTime	10
    # UserRemoteAddr	31.185.7.89
    # UserRemoteUserAgent	Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36
    # UserSessionStart	2020-09-19 22:33:52 / 0 h
    # UserShowTickets	25
    # UserTimeZone	Europe/Moscow
    # UserTitle	esiatest004@yandex.ru
    # UserToken	lF4TLaMuz0hG0g
    # UserType	Customer
    # ValidID	1
