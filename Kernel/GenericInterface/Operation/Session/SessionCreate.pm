# --
# Copyright (C) 2001-2020 OTRS AG, https://otrs.com/
# --
# This software comes with ABSOLUTELY NO WARRANTY. For details, see
# the enclosed file COPYING for license information (GPL). If you
# did not receive this file, see https://www.gnu.org/licenses/gpl-3.0.txt.
# --

package Kernel::GenericInterface::Operation::Session::SessionCreate;

use strict;
use warnings;

use Kernel::System::VariableCheck qw(IsStringWithData IsHashRefWithData);

use parent qw(
    Kernel::GenericInterface::Operation::Common
    Kernel::GenericInterface::Operation::Session::Common
);

use POSIX;
use JSON::XS;
use Redis;

our $ObjectManagerDisabled = 1;

=head1 NAME

Kernel::GenericInterface::Operation::Session::SessionCreate - GenericInterface Session Create Operation backend

=head1 PUBLIC INTERFACE

=head2 new()

usually, you want to create an instance of this
by using Kernel::GenericInterface::Operation->new();

=cut

sub new {
    my ( $Type, %Param ) = @_;

    my $Self = {};
    bless( $Self, $Type );

    # check needed objects
    for my $Needed (
        qw(DebuggerObject WebserviceID)
        )
    {
        if ( !$Param{$Needed} ) {

            return {
                Success      => 0,
                ErrorMessage => "Got no $Needed!"
            };
        }

        $Self->{$Needed} = $Param{$Needed};
    }

    $Self->{Redis} = Redis->new();

    return $Self;
}

=head2 Run()

Retrieve a new session id value.

    my $Result = $OperationObject->Run(
        Data => {
            UserLogin         => 'Agent1',
            CustomerUserLogin => 'Customer1',       # optional, provide UserLogin or CustomerUserLogin
            Password          => 'some password',   # plain text password
        },
    );

    $Result = {
        Success      => 1,                                # 0 or 1
        ErrorMessage => '',                               # In case of an error
        Data         => {
            SessionID => $SessionID,
        },
    };

=cut

sub Run {
    my ( $Self, %Param ) = @_;

    # check needed stuff
    if ( !IsHashRefWithData( $Param{Data} ) ) {

        return $Self->ReturnError(
            ErrorCode    => 'SessionCreate.MissingParameter',
            ErrorMessage => "SessionCreate: The request is empty!",
        );
    }

    if ($Param{Data}->{BlitzLogin} || $Param{Data}->{BlitzToken}) {
        for my $Needed (qw( BlitzLogin BlitzToken )) {
            if ( !$Param{Data}->{$Needed} ) {

                return $Self->ReturnError(
                    ErrorCode    => 'SessionCreate.MissingParameter',
                    ErrorMessage => "SessionCreate: $Needed parameter is missing!",
                );
            }
        }

        my @keys = $Self->{Redis}->keys('*');
        my $OK = 0;
        for my $key (@keys) {
            my $User = decode_json $Self->{Redis}->get($key);

            if (
                ($Param{Data}->{BlitzToken} eq $User->{BlitzToken} ||
                 $Param{Data}->{BlitzToken} eq $User->{blitz_token}) && (
                 $Param{Data}->{BlitzLogin} eq $User->{BlitzLogin} ||
                 $Param{Data}->{BlitzLogin} eq $User->{email}) ) {
                   $OK = 1; 
                }
        }

        return $Self->ReturnError(
            ErrorCode    => 'SessionCreate.MissingParameter',
            ErrorMessage => "SessionCreate: BlitzToken or BlitzLogin is invalid!",
        ) unless $OK;

        my $SessionObject = $Kernel::OM->Get('Kernel::System::AuthSession');

        my $Session = $SessionObject->CreateSessionID(
            ChangeBy => 1,
            ChangeTime => strftime("%F %T", localtime $^T),
            CompanyConfig => {},
            Config => {},
            CreateBy => 1,
            CreateTime => strftime("%F %T", localtime $^T),
            CustomerCompanyName => 'Внешние пользователи',
            CustomerCompanyValidID => 1,
            CustomerID => 'Внешние пользователи',
            SessionSource => 'CustomerInterface',
            Source => 'Blitz',
            UserCustomerID => 'Внешние пользователи',
            UserEmail => $Param{Data}->{BlitzLogin},
            UserFirstname => $Param{Data}->{BlitzLogin},
            UserFullname => sprintf("%s %s %s", '', '', ''),
            UserID => $Param{Data}->{BlitzLogin},
            UserLanguage => 'ru',
            UserLastname => $Param{Data}->{BlitzLogin},
            UserLogin => $Param{Data}->{BlitzLogin},
            UserMailString => sprintf("%s %s <%s>", '', '', ''),
            UserPassword => '[xxx]',
            UserRefreshTime => 10,
            UserRemoteAddr => '31.185.7.89',
            UserTitle => $Param{Data}->{BlitzLogin},
            UserSessionStart => time(),
            UserShowTickets => '25',
            UserTimeZone => 'Europe/Moscow',
            UserType  => 'Customer',
            UserLastRequest => time(),
            UserZip => '',
            ValidID => 0,
        );
        return {
            Success => 1,
            Data    => {
                SessionID => $Session,
            },
        };
    }
    # BlitzLogin BlitzToken

    for my $Needed (qw( Password )) {
        if ( !$Param{Data}->{$Needed} ) {

            return $Self->ReturnError(
                ErrorCode    => 'SessionCreate.MissingParameter',
                ErrorMessage => "SessionCreate: $Needed parameter is missing!",
            );
        }
    }

    my $SessionID = $Self->CreateSessionID(
        %Param,
    );

    if ( !$SessionID ) {

        return $Self->ReturnError(
            ErrorCode    => 'SessionCreate.AuthFail',
            ErrorMessage => "SessionCreate: Authorization failing!",
        );
    }

    return {
        Success => 1,
        Data    => {
            SessionID => $SessionID,
        },
    };
}

1;

=head1 TERMS AND CONDITIONS

This software is part of the OTRS project (L<https://otrs.org/>).

This software comes with ABSOLUTELY NO WARRANTY. For details, see
the enclosed file COPYING for license information (GPL). If you
did not receive this file, see L<https://www.gnu.org/licenses/gpl-3.0.txt>.

=cut
