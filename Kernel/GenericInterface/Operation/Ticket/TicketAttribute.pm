# --
# Copyright (C) 2001-2020 OTRS AG, https://otrs.com/
# --
# This software comes with ABSOLUTELY NO WARRANTY. For details, see
# the enclosed file COPYING for license information (GPL). If you
# did not receive this file, see https://www.gnu.org/licenses/gpl-3.0.txt.
# --

package Kernel::GenericInterface::Operation::Ticket::TicketAttribute;

use strict;
use warnings;

use Kernel::System::VariableCheck qw(IsStringWithData IsHashRefWithData);

use parent qw(
    Kernel::GenericInterface::Operation::Common
    Kernel::GenericInterface::Operation::Ticket::Common
);

use POSIX;
use JSON::XS;
use Redis;

our $ObjectManagerDisabled = 1;

=head1 NAME

Kernel::GenericInterface::Operation::Ticket::TicketAttribute - returns attributes list

=head1 PUBLIC INTERFACE

=head2 new()

by using Kernel::GenericInterface::Operation->new();

=cut

sub new {
    my ( $Type, %Param ) = @_;

    my $Self = {};
    bless( $Self, $Type );

    # check needed objects
    for my $Needed (
        qw(DebuggerObject WebserviceID)
        )
    {
        if ( !$Param{$Needed} ) {

            return {
                Success      => 0,
                ErrorMessage => "Got no $Needed!"
            };
        }

        $Self->{$Needed} = $Param{$Needed};
    }

    return $Self;
}

=head2 Run()

Retrieve a new session id value.

    my $Result = $OperationObject->Run(
        Data => {
            UserLogin         => 'Agent1',
            CustomerUserLogin => 'Customer1',       # optional, provide UserLogin or CustomerUserLogin
            Password          => 'some password',   # plain text password
        },
    );

    $Result = {
        Success      => 1,                                # 0 or 1
        ErrorMessage => '',                               # In case of an error
        Data         => {
            SessionID => $SessionID,
        },
    };

=cut

sub Run {
    my ( $Self, %Param ) = @_;

    # my $Service = $Kernel::OM->Get('Kernel::System::Service')->ServiceList(UserID => 1);
    my %Params = ( UserID => 1, Valid => 1);
    my $df = $Kernel::OM->Get('Kernel::System::DynamicField');

    my %ServiceList = $Kernel::OM->Get('Kernel::System::Service')->ServiceList(%Params);
    $ServiceList{$_} =~ s/::/:/ for keys %ServiceList;

    return {
        Success => 1,
        Data    => {
            Service => \%ServiceList,
            Queue   => { $Kernel::OM->Get('Kernel::System::Queue')->QueueList(%Params) },
            Type    => { $Kernel::OM->Get('Kernel::System::Type')->TypeList(%Params) },
            Priority=> { $Kernel::OM->Get('Kernel::System::Priority')->PriorityList(%Params) },
            DynamicField => { map { $_->{ID} => $_ } @{ $df->DynamicFieldListGet() } },
        },
    };
}

1;

=head1 TERMS AND CONDITIONS

This software is part of the OTRS project (L<https://otrs.org/>).

This software comes with ABSOLUTELY NO WARRANTY. For details, see
the enclosed file COPYING for license information (GPL). If you
did not receive this file, see L<https://www.gnu.org/licenses/gpl-3.0.txt>.

=cut
