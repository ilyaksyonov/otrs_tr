# --
# Copyright (C) 2001-2017 OTRS AG, http://otrs.com/
# --
# This software comes with ABSOLUTELY NO WARRANTY. For details, see
# the enclosed file COPYING for license information (AGPL). If you
# did not receive this file, see http://www.gnu.org/licenses/agpl.txt.
# --

package Kernel::System::Stats::Static::CustomStat1;

use strict;
use warnings;
use utf8;
use Data::Dumper;
use Encode;
use URI::Escape;
use experimental 'smartmatch';
no warnings 'experimental::smartmatch';

our @ObjectDependencies = (
    'Kernel::Language',
    'Kernel::System::DB',
    'Kernel::System::DateTime',
);

sub new {
    my ( $Type, %Param ) = @_;

    # allocate new hash for object
    my $Self = {};
    bless( $Self, $Type );

    return $Self;
}

sub GetObjectName {
    return 'CustomStat1';
}

sub GetColumns {
    return [ 'Заявка №','Способ связи','Название','Создана','Время закрытия', 'Очередь','Статус','Приоритет','Исполнитель','Клиент', 'Организация','Регион'];
}

sub Param {
    my $Self = shift;

    return ({
        Frontend         => 'Period',
        Name             => 'Period',
        Multiple         => 0,
        Size             => 0,
        SelectedID       => 'week',
        Data           => { week => 'week', day => 'day' }
    });
}

sub Run {
    my ( $Self, %Param ) = @_;

    # sudo -u otrs ./otrs.Console.pl Maint::Stats::Generate 
    #--mail-recipient ilyaksyonov@gmail.com
    #--mail-recipient g.malakhov@ctrl2go.com
    #--mail-recipient a.iskhizova@ctrl2go.com
    #--mail-recipient natalia.sergeeva@fors.ru
    #--mail-recipient o.aksenova@ctrl2go.com
    # --number 10075 --mail-body "the previous day stats" --format 'Excel' --params Period=day


    $Param{Period} ||= 'day';
    my $period_in_days = $Param{Period} eq 'day' ? 1 : 7;

    my $LanguageObject = $Kernel::OM->Get('Kernel::Language');
    my $SQL = qq{
        select
            t.tn,
            case 
                when cc2.name in ('Phone', 'Internal') then 'телефон'
                else cc2.name
            end,
            t.title,
            t.create_time,
            case when t.escalation_solution_time > 0
                then FROM_UNIXTIME(t.escalation_solution_time)
                else NULL end,
            q.name,
            case
                when ts.name = 'closed successful' then 'выполнено'
                when ts.name = 'new'  then 'создано'
                when ts.name = 'pending reminder' then 'в ожидании'
                when ts.name = 'open' then 'в работе'
                else ts.name
            end,
            tp.name,
			concat(u2.first_name, ' ', u2.last_name),
            concat(cu.last_name, " ", cu.first_name, " <", cu.email, ">"),
            cc.name,
            dfv_region.value_text
        from ticket t
        inner join users u2 on t.user_id = u2.id
        inner join article a2 on a2.ticket_id = t.id and
                                a2.id = (select id from article where ticket_id = t.id order by id asc limit 1)
        inner join communication_channel cc2 on cc2.id = a2.communication_channel_id 
        inner join queue q on t.queue_id = q.id 
        inner join ticket_state ts on t.ticket_state_id = ts.id
        inner join ticket_priority tp on tp.id = t.ticket_priority_id 
        left join customer_user cu on cu.login = t.customer_user_id
        left join customer_company cc on cc.customer_id = t.customer_id
        left join dynamic_field_value dfv_region on dfv_region.object_id = t.id and dfv_region.field_id = 6
        where t.queue_id not in (3, 8) and t.create_time >= DATE(NOW()) - INTERVAL $period_in_days DAY
    };


    my $DynamicFieldObject        = $Kernel::OM->Get('Kernel::System::DynamicField');
    my $DynamicFieldBackendObject = $Kernel::OM->Get('Kernel::System::DynamicField::Backend');

    my $Region = $DynamicFieldObject->DynamicFieldGet(
        ID   => 6 #Name => 'Region',
    );

    # $Self->log($Region);

    my $ResultAsArrayRef = $Kernel::OM->Get('Kernel::System::DB')->SelectAll(
        SQL   => $SQL,
    );

    for my $row (@$ResultAsArrayRef) {
        next unless $row->[11]; 
        $row->[11] = $Region->{Config}->{PossibleValues}->{$row->[11]} || '';
    }

    my $Title = "";
    return ( [$Title], $Self->GetColumns, @$ResultAsArrayRef );
}

sub log {
    my ($Self, $msg) = @_;

    $Kernel::OM->Get('Kernel::System::Log')->Log(
        Priority => 'error',
        Message => Dumper($msg)
    );
}

1;
