# --
# Copyright (C) 2001-2017 OTRS AG, http://otrs.com/
# --
# This software comes with ABSOLUTELY NO WARRANTY. For details, see
# the enclosed file COPYING for license information (AGPL). If you
# did not receive this file, see http://www.gnu.org/licenses/agpl.txt.
# --

package Kernel::System::Stats::Dynamic::CustomStat1;

use strict;
use warnings;
use utf8;
use Data::Dumper;
use Encode;
use URI::Escape;
use experimental 'smartmatch';
no warnings 'experimental::smartmatch';

our @ObjectDependencies = (
    'Kernel::Language',
    'Kernel::System::DB',
    'Kernel::System::DateTime',
);

sub new {
    my ( $Type, %Param ) = @_;

    # allocate new hash for object
    my $Self = {};
    bless( $Self, $Type );

    return $Self;
}

sub GetObjectName {
    return 'CustomStat1';
}

sub GetColumns {
    return [ 'Номер','Описание','Состояние','Тип заявки','Сервис', 'Контрагент / автор','Ответственный','Количество возобновлений' ];
}

sub Param {
    my $Self = shift;

    my $UserObject            = $Kernel::OM->Create('Kernel::System::User');
    my $QueueObject           = $Kernel::OM->Create('Kernel::System::Queue');
    my $DateTimeObject        = $Kernel::OM->Create('Kernel::System::DateTime');
    my $CustomerCompanyObject = $Kernel::OM->Create('Kernel::System::CustomerCompany');
    my $StateObject           = $Kernel::OM->Create('Kernel::System::State');
    my $ServiceObject         = $Kernel::OM->Create('Kernel::System::Service');

    # $DateTimeObject->Subtract( Months => 1 );
    my $DateTimeSettings = $DateTimeObject->Get();

    my %Year = map { $_ => $_; } ( $DateTimeSettings->{Year} - 10 .. $DateTimeSettings->{Year} );
    my %Month = map { $_ => sprintf( "%02d", $_ ); } ( 1 .. 12 );
    my %Day = map { $_ => sprintf( "%02d", $_ ); } ( 1 .. 31 );
    my %CustomerCompanyList = $CustomerCompanyObject->CustomerCompanyList( Limit => 9999 );
    my %UserList = $UserObject->UserList(
        Type          => 'Long',
        Valid         => 1,
        NoOutOfOffice => 1,
    );
    my %Queues = $QueueObject->GetAllQueues();

    my %States = $StateObject->StateList(
        UserID => 1,
        Valid  => 1, # is default
    );

    my $ServiceList = $ServiceObject->ServiceListGet(
        Valid  => 1,   # (optional) default 1 (0|1)
        UserID => 1,
    );

    my %Servcies = map { $_->{ServiceID} => $_->{Name} } @$ServiceList;


    my @Params = (
        {
            Frontend   => 'Start Year',
            Name       => 'StartYear',
            Multiple   => 0,
            Size       => 0,
            SelectedID => $DateTimeSettings->{Year},
            Data       => \%Year,
        },
        {
            Frontend   => 'Start Month',
            Name       => 'StartMonth',
            Multiple   => 0,
            Size       => 0,
            SelectedID => $DateTimeSettings->{Month},
            Data       => \%Month,
        },
        {
            Frontend   => 'Start Day',
            Name       => 'StartDay',
            Multiple   => 0,
            Size       => 0,
            SelectedID => $DateTimeSettings->{Day},
            Data       => \%Day,
        },
        {
            Frontend   => 'End Year',
            Name       => 'EndYear',
            Multiple   => 0,
            Size       => 0,
            SelectedID => $DateTimeSettings->{Year},
            Data       => \%Year,
        },
        {
            Frontend   => 'End Month',
            Name       => 'EndMonth',
            Multiple   => 0,
            Size       => 0,
            SelectedID => $DateTimeSettings->{Month},
            Data       => \%Month,
        },
        {
            Frontend   => 'End Day',
            Name       => 'EndDay',
            Multiple   => 0,
            Size       => 0,
            SelectedID => $DateTimeSettings->{Day},
            Data       => \%Day,
        },
        {
            Frontend   => 'Service',
            Name       => 'Service',
            Multiple   => 1,
            Size       => 0,
            SelectedID => '',
            Data       => \%Servcies,
        },
        {
            Frontend   => 'Customer',
            Name       => 'Customer',
            Multiple   => 1,
            Size       => 0,
            SelectedID => '',
            Data       => \%CustomerCompanyList,
        },
        {
            Frontend   => 'User',
            Name       => 'User',
            Multiple   => 1,
            Size       => 0,
            SelectedID => '',
            Data       => \%UserList,
        },
        {
            Frontend   => 'Queue',
            Name       => 'Queue',
            Multiple   => 1,
            Size       => 0,
            SelectedID => [1,2],
            Data       => \%Queues,
        },
        {
            Frontend   => 'State',
            Name       => 'State',
            Multiple   => 1,
            Size       => 0,
            SelectedID => '',
            Data       => \%States,
        },
        {
            Frontend   => 'Only with renewal',
            Name       => 'OnlyWithRenewal',
            Multiple   => 0,
            Size       => 0,
            SelectedID => '',
            Data       => {1 => 'да',0 => 'нет'},
        },
        {
            Frontend   => 'Order by',
            Name       => 'OrderBy',
            Multiple   => 0,
            Size       => 0,
            SelectedID => '',
            Data       => $Self->GetColumns,
        },
        {
            Frontend         => 'Sort sequence',
            Name             => 'SortSequence',
            Multiple         => 0,
            Size             => 0,
            SelectedID       => '',
            Data           => { ASC   => 'ascending', DESC => 'descending' }
        },
    );
    return @Params;
}

sub Run {
    my ( $Self, %Param ) = @_;

    for (qw/StartYear StartMonth StartDay EndYear EndMonth EndDay Queue/ ) {
        my %helper = (
            StartYear  => '2020',
            StartMonth => '01',
            StartDay   => '01',
            EndYear    => '2020',
            EndMonth   => '01',
            EndDay     => '01',
        );

        if (not defined $Param{$_}) {
            $Param{$_} = $helper{$_} || 'not exists';

            $Kernel::OM->Get('Kernel::System::Log')->Log(
                Priority => 'error',
                Message  => "Param $_ not found, set default value ($helper{$_} $Param{$_})",
            );
        }
        # $Kernel::OM->Get('Kernel::System::Log')->Log(
        #     Priority => 'error',
        #     Message  => "Param $_ not found",
        # ) and die if not defined $Param{$_};
    }

    # for (qw/Customer User/ ) {
        # $Kernel::OM->Get('Kernel::System::Log')->Log(
        #     Priority => 'error',
        #     Message  => Dumper \%Param,
        # );
    #     ) and die if not @{$Param{$_}};
    # }

    my $date_start = $Param{StartYear}.'-'.$Param{StartMonth}.'-'.$Param{StartDay}.' 00:00:00';
    my $date_end = $Param{EndYear}.'-'.$Param{EndMonth}.'-'.$Param{EndDay}.' 23:59:59';

    my $LanguageObject = $Kernel::OM->Get('Kernel::Language');
    my $SQL = q{
        SELECT
            ticket.tn as "Номер",
            ticket.title as "Описание",
            ticket_state.name as "Состояние",
            ticket_type.name as "Тип заявки",
            service.name as "Сервис",
            CONCAT(customer_company.name, ' / ', customer_user.first_name, ' ', customer_user.last_name) as "Контрагент / автор",
            CONCAT(users.first_name, ' ', users.last_name) as "Ответственный",
            dfv_2.value_text as "Количество возобновлений"
        FROM
            ticket
            LEFT JOIN customer_company ON customer_company.customer_id = ticket.customer_id
            LEFT JOIN customer_user ON customer_user.email = ticket.customer_user_id
            LEFT JOIN service ON service.id = ticket.service_id
            JOIN users ON users.id = ticket.user_id
            JOIN ticket_type ON ticket_type.id = ticket.type_id
            JOIN ticket_state ON ticket_state.id = ticket.ticket_state_id
            LEFT JOIN (
                SELECT value_text, object_id FROM dynamic_field_value where field_id = 16 and value_text is not null) AS dfv_2
            ON dfv_2.object_id = ticket.id
    };

    $SQL .= " WHERE ticket.create_time BETWEEN '$date_start' AND '$date_end' AND ticket.type_id != 7 ";
    $SQL .= " AND ticket.user_id in (". (join ', ', @{$Param{User}}) .")" if @{$Param{User}};
    $SQL .= " AND ticket.queue_id in (". (join ', ', @{$Param{Queue}}) .")" if @{$Param{Queue}};
    $SQL .= " AND ticket.customer_id in (". (join ', ', @{$Param{Customer}}) .")" if @{$Param{Customer}};
    $SQL .= " AND ticket.ticket_state_id in (". (join ', ', @{$Param{State}}) .")" if @{$Param{State}};
    $SQL .= " AND ticket.service_id in (". (join ', ', @{$Param{Service}}) .")" if @{$Param{Service}};

    if ($Param{Preview}) {
        $Param{OrderBy} = Encode::decode('utf8', uri_unescape($Param{OrderBy}));
    }
    $SQL .= " ORDER BY `$Param{OrderBy}` $Param{SortSequence}";

    my $DBObject = $Kernel::OM->Get('Kernel::System::DB');
    my $ResultAsArrayRef = $DBObject->SelectAll(
        SQL   => $SQL,
    );

    my $count = 0;
    my $all = @{$ResultAsArrayRef};
    for my $row (@{$ResultAsArrayRef}) {
        $row->[2] = $LanguageObject->Translate($row->[2]);
        $row->[3] = $LanguageObject->Translate($row->[3]);
        $row->[4] = $LanguageObject->Translate($row->[4]);
        $count++ if $row->[7];
    }
    my $persent = eval {int($count*100/$all)} || 0;


    if ($Param{OnlyWithRenewal}) {
        @$ResultAsArrayRef = grep {int($_->[7] || 0) > 0} @$ResultAsArrayRef;
        $all = @$ResultAsArrayRef;
        $persent = 100;
    }
    # use Data::Dumper;
    # $Kernel::OM->Get('Kernel::System::Log')->Log(
    #     Priority => 'notice',
    #     Message  => Dumper $ResultAsArrayRef,
    # );

    # push @$ResultAsArrayRef, ["ИТОГО: Количество возобновленных заявок $count ($persent%)  из общего количества $all"];
    # if (!$Param{Preview} && $Param{Format} ne 'Print') {
    #     unshift @$ResultAsArrayRef, ["Период: ", "$date_start - $date_end"];
    # }
    push @$ResultAsArrayRef, ["ИТОГО:"];
    push @$ResultAsArrayRef, ["Количество возобновленных заявок", "$count ($persent%)"];
    push @$ResultAsArrayRef, ["Общее количество", $all];

    if (!$Param{Preview} && $Param{Format} ne 'Print') {
        push @$ResultAsArrayRef, [];
        push @$ResultAsArrayRef, ["Параметры фильтра:"];
        push @$ResultAsArrayRef, [
            "Период: ",
            sprintf(
                "%02d.%02d.%d 00:00 - %02d.%02d.%d 23:59",
                $Param{StartDay},
                $Param{StartMonth},
                $Param{StartYear},
                $Param{EndDay},
                $Param{EndMonth},
                $Param{EndYear}
        )];
        for my $key ( keys %Param ) {
            next if $key =~ /(Day|EndYear|OrderBy|Month|Year|EndMonth|EndDay|StartYear|StartMonth|StartDay|SortSequence|Format|Module)/;

            my $str;
            my $OnlyWithRenewal = delete $Param{'OnlyWithRenewal'};
            if ( $key eq 'OnlyWithRenewal' || @{ $Param{$key} }) {

            if ( $key eq 'Service' ) {
                my $ServiceObject = $Kernel::OM->Create('Kernel::System::Service');
                my $ServiceList   = $ServiceObject->ServiceListGet(
                    Valid  => 1,   # (optional) default 1 (0|1)
                    UserID => 1,
                );

                $str = join ', ', map { $_->{Name} } grep {$_->{ServiceID} ~~ @{ $Param{$key} } } @$ServiceList;
            } elsif ($key eq 'Customer') {
                my $CustomerCompanyObject = $Kernel::OM->Create('Kernel::System::CustomerCompany');
                my %CustomerCompanyList = $CustomerCompanyObject->CustomerCompanyList;
                $str = join ', ', map { $CustomerCompanyList{$_} } grep { $_ ~~ @{ $Param{$key} } } keys %CustomerCompanyList;
            } elsif ($key eq 'Queue') {
                my $QueueObject           = $Kernel::OM->Create('Kernel::System::Queue');
                my %Queues = $QueueObject->GetAllQueues();
                $str = join ', ', map { $Queues{$_} } grep { $_ ~~ @{ $Param{$key} } } keys %Queues;
            } elsif ($key eq 'User') {
                my $UserObject            = $Kernel::OM->Create('Kernel::System::User');
                my %UserList = $UserObject->UserList(
                    Type          => 'Long',
                    Valid         => 1,
                    NoOutOfOffice => 1,
                );
                $str = join ', ', map { $UserList{$_} } grep { $_ ~~ @{ $Param{$key} } } keys %UserList;
            } elsif ($key eq 'Type') {
                my $TypeObject = $Kernel::OM->Create('Kernel::System::Type');
                my %Types = $TypeObject->TypeList();
                $str = join ', ', map { $LanguageObject->Translate( $Types{$_} ) } grep { $_ ~~ @{ $Param{$key} } } keys %Types;
            } elsif ($key eq 'State') {
                my $StateObject           = $Kernel::OM->Create('Kernel::System::State');
                my %States = $StateObject->StateList(
                    UserID => 1,
                    Valid  => 1, # is default
                );
                $str = join ', ', map { $LanguageObject->Translate( $States{$_} ) } grep { $_ ~~ @{ $Param{$key} } } keys %States;
            } elsif ($key eq 'OnlyWithRenewal') {
                $key = $LanguageObject->Translate('Only with renewal');
                $str = ($Param{$key}) ? 'Да' : 'Нет';
            }
            } else {
                $str = "Все";
            }
            push @$ResultAsArrayRef, [$LanguageObject->Translate($key), $str];
            #push @$ResultAsArrayRef, [ $LanguageObject->Translate($key), $str];
        }
    }

    my $Title = "";
    return ( [$Title], [ 'Номер','Описание','Состояние','Тип заявки','Сервис','Контрагент / автор','Ответственный','Количество возобновлений' ], @$ResultAsArrayRef );
}

1;
