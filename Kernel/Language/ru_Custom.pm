# --
# Copyright (C) 2001-2020 OTRS AG, https://otrs.com/
# --
# This software comes with ABSOLUTELY NO WARRANTY. For details, see
# the enclosed file COPYING for license information (GPL). If you
# did not receive this file, see https://www.gnu.org/licenses/gpl-3.0.txt.
# --

package Kernel::Language::ru_Custom;

use strict;
use warnings;
use utf8;

sub Data {
    my $Self = shift;

    $Self->{Translation}->{'Region'} = 'Регион';
    $Self->{Translation}->{'Section'} = 'Раздел';
    $Self->{Translation}->{'New Ticket 2'} = 'Новое обращение';
    $Self->{Translation}->{'Tickets'} = 'Обращения';
    $Self->{Translation}->{'My Tickets'} = 'Обращения';
    $Self->{Translation}->{'Last name'} = 'Фамилия';
    $Self->{Translation}->{'Middle name'} = 'Отчество';
    $Self->{Translation}->{'First name'} = 'Имя';
    $Self->{Translation}->{'Position'} = 'Должность';
    $Self->{Translation}->{'open'} = 'в работе';
    $Self->{Translation}->{'pending reminder'} = 'в ожидании';
    $Self->{Translation}->{'reopened'} = 'повторная';
    # $Self->{Translation}->{''} = '';
}


1;
